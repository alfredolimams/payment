import java.util.Scanner;

public class Command {

	private Scanner input;
	private Manager manager;
	private int idx;
	private Command after;
	private Command before;
	public Command(Scanner input) {
		// TODO Auto-generated constructor stub

		this.input = input;
		this.manager = new Manager();
		this.idx = 0;
		
		this.after = null;
		this.before = null;
		
	}

	public void read() {
		int command = input.nextInt();
		if (command < 1 || command > 10) {
			invalid();
		} else {

			if (command == 1) {

				addWorker();
				
			} else if (command == 2) {

				removeWorker();
				
			} else if (command == 3) {

				point();
				
			} else if (command == 4) {

				sell();
				
			} else if (command == 5) {

				services();
				
			} else if (command == 6) {

				changeWorker();
				
			} else if (command == 7) {

				payment();
				
			} else if (command == 8) {

				undoRedo();
				
			} else if (command == 9) {

			} else if (command == 10) {

			}
		}
	}

	public void menu() {
		System.out.println(" Sistema: ");
		System.out.print(" 1 - Adicionar Funcionário ");
		System.out.print(" 2 - Remover Funcionário ");
		System.out.print(" 3 - Cartão de Ponto ");
		System.out.print(" 4 - Resultado de Venda ");
		System.out.println(" 5 - Taxa de Serviço ");
		System.out.print(" 6 - Detalhes do Funcionário ");
		System.out.print(" 7 - Folha de Pagamento ");
		System.out.print(" 8 - Desfazer/Refazer ");
		System.out.print(" 9 - Agenda de Pagamento ");
		System.out.println(" 10 - Nova Agenda ");
	}

	public void invalid() {
		System.out.println("Comando Inválido");
	}

	private void addWorker() {
		String name;
		String cpf;
		String address;
		System.out.print(" Name: ");
		name = input.next();
		System.out.print(" CPF: ");
		cpf = input.next();
		if( manager.searchCpf(cpf) != null ){
			System.out.print(" CPF cadastrado ");
			return;
		} 
			
		System.out.print(" Endereço: ");
		address = input.next();
		String id = ("a" + (idx++));
		System.out.println(" Tipo de Funcionário: ");
		System.out.println(" 1 - Assalariado, 2 - Vendedor, 3 - Horista ");
		int typeWorker;
		while ((typeWorker = input.nextInt()) > 3 || typeWorker < 1) {
			invalid();
		}
		System.out.println(" Digite o salário ");
		float salary = input.nextFloat();
		System.out.println(" Tipo de Pagamento: ");
		System.out
				.println(" 1 - Cheque pelos Correios , 2 - Cheque em mãos , 3 - Depósito Bancário ");
		int typePayment;
		while ((typePayment = input.nextInt()) > 3 || typeWorker < 1) {
			invalid();
		}
		
		// Save status actual
		before = this;
		after = null;
		Workers worker;
		if (typeWorker == 1) {
			
			worker = new Salaried( name, cpf, address, id, salary, typePay(typePayment) );
			
		} else if (typeWorker == 2) {

			System.out.print(" Digite a porcetagem de comissão ");
			float rate = input.nextFloat();
			worker = new Seller( name, cpf, address, id, salary, typePay(typePayment), rate );
			
		} else {

			worker = new Hourly( name, cpf, address, id, salary, typePay(typePayment) );
			
		}
		
		if( manager.addWorker( worker ) ){
			
			System.out.println(" Cadastro do funcionário concluído ");
			
		}
		else{
			
			System.out.println("Cadastro falhou");
			
		}
	}

	private String typePay(int i) {

		if (i == 1) {
			return "Cheque pelos Correios";
		} else if (i == 2) {
			return "Cheque em mãos";
		} else {
			return "Depósito Bancário";
		}
	}
	
	private void removeWorker() {
		String cpf;
		System.out.print(" Digite o CPF: ");
		cpf =  input.next();
		
		if( manager.searchCpf(cpf) == null ){
			System.out.println(" CPF não cadastrado ");
			return;
		}
		before = this;
		after = null;
		if( manager.removeWorker( cpf ) ){
			System.out.println(" Funcionário removido com sucesso ");
		}
	}
	
	private void changeWorker() {
		
		String cpf;
		System.out.print(" Digite o CPF: ");
		cpf =  input.next();
		Workers worker;
		if( ( worker = manager.searchCpf(cpf) ) != null ){
			
			System.out.println(" Alterar: ");
			System.out.println(" 1 - Nome, 2 - Endereço, 3 - Salário ");
			System.out.print(" 4 - CPF, 5 - Tipo de Pagamento");
			if( worker instanceof Seller ){
				System.out.print(", 6 - Comissão ");
			}
			int change;
			while ((change = input.nextInt()) > 6 && worker instanceof Seller 
					|| change > 5 || change < 1) {
				invalid();
			}
			
			before = this;
			
			if( change == 1 ){
				System.out.print(" Nome: ");
				String name = input.next();
				worker.name = name;
			} else if( change == 2 ){
				System.out.print(" Endereço: ");
				String address = input.next();
				worker.address = address;
			} else if( change == 3 ) {
				System.out.print(" Salário: ");
				float salary = input.nextFloat();
				worker.salary = salary;				
			} else if( change == 5 ){
				System.out.println(" Tipo de Pagamento: ");
				System.out
						.println(" 1 - Cheque pelos Correios , 2 - Cheque em mãos , 3 - Depósito Bancário ");
				int type = input.nextInt();
				worker.payment = typePay(type);	
			} else if( change == 4 ){
				System.out.print(" CPF: ");
				String newCpf = input.next();
				worker.cpf = newCpf;
				manager.removeWorker(cpf);
				manager.addWorker(worker);
			} else {
				Seller seller = (Seller)worker;
				System.out.print(" Comissão: ");
				float rate = input.nextFloat();
				seller.setRate(rate);
			}
		}
	}
	
	private void sell(){
		
		System.out.println(" CPF do vendedor: ");
		String cpf = input.nextLine();
		Workers worker = manager.searchCpf(cpf);
		if( worker instanceof Seller ){
			
			before = this;
			after = null;
			System.out.print("Valor da venda: ");
			float price = input.nextFloat();
			Seller seller = (Seller)worker;
			seller.receive += seller.getRate()*price;
				
		} else {
			
			if( worker != null )
				System.out.println("Não é vendedor");
			else
				System.out.println("Nenhum vendedor com esse CPF");
		}
		
	}

	private void services(){
		
		System.out.println(" CPF do funcionário: ");
		String cpf = input.nextLine();
		Workers worker = manager.searchCpf(cpf);
		if( worker != null ){
			
			before = this;
			after = null;
			System.out.print("Valor do serviço ");
			float price = input.nextFloat();
			worker.services += price;
				
		} else {
			
			System.out.println("Nenhum vendedor com esse CPF");
			
		}
		
	}
	
	private void point(){
		System.out.println(" CPF do funcionário: ");
		String cpf = input.next();
		Workers worker = manager.searchCpf(cpf);
		if( worker != null ){
			
			if( worker instanceof Hourly ){
				
				before = this;
				after = null;
				manager.cardsPoint(cpf, new Events() );
				System.out.println( new Events() );
				
			} else{
				
				System.out.println("Não é horista");
			}
				
		} else {
			
			System.out.println("Nenhum vendedor com esse CPF");
			
		}
	}

	private void undo(){
		
		if( before != null ){
			
			after = this;
			// Think ( this = before; )
			manager = before.manager;
			idx = before.idx;
			before = before.before;
			
		} else {
			
			System.out.println(" Não pode desfazer ");
			
		}
		
	}

	private void redo(){
		
		if( after != null ){
			
			before = this;
			// Think( this = after; ) 
			manager = after.manager;
			idx = after.idx;
			before = after.before;
			
		} else {
			
			System.out.println(" Não pode refazer ");
			
		}
		
	}
	
	private void undoRedo() {
		
		System.out.println(" 1 - Desfazer , 2 - Refazer ");
		int type;
		while ((type = input.nextInt()) > 2 || type < 1) {
			invalid();
		}
		
		if( type == 1 ){
			undo();
		} else {
			redo();
		}
		
	}
	
	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public Command getAfter() {
		return after;
	}

	public void setAfter(Command after) {
		this.after = after;
	}

	public Command getBefore() {
		return before;
	}

	public void setBefore(Command before) {
		this.before = before;
	}
	
	public void payment(){
		manager.payment();
	}
	
}

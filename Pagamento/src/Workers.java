
public abstract class Workers extends People {

	protected String id;
	protected double salary;
	protected String payment;
	protected float receive;
	protected float services;
	
	public Workers( String name, String cpf , String address, String id, double salary, String payment ) {
		
		super(name, cpf, address);
		this.id = id;
		this.salary  = salary;
		this.payment = payment;
		this.receive = 0;
		this.services = 0;
		
	}
	
	public abstract double pay();
	
}

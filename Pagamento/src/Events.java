import java.util.Date;
import java.util.GregorianCalendar;

public class Events {

	private GregorianCalendar date;
	
	public GregorianCalendar getDate() {
		return date;
	}

	public void setDate(GregorianCalendar date) {
		this.date = date;
	}

	public Events( ){
		
		date = new GregorianCalendar();
		
	}
	
	@SuppressWarnings("deprecation")
	public int variation( Events event ){
		
		Date timer1 = date.getTime();
		Date timer2 = event.getDate().getTime();
		
		int minutes1 = timer1.getMinutes(); 
		int minutes2 = timer2.getMinutes();
		
		return Math.abs( minutes1 - minutes2 );
	}
	
}


import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Manager {

	private Map<String, Workers> workers;
	private Map<String, LinkedList<Events> > cardsPoint; 
	public Manager() {

		workers = new HashMap<String, Workers>();
		cardsPoint = new HashMap< String, LinkedList<Events> >();
	}

	public boolean addWorker(Workers worker) {

		if (workers.containsKey(worker.cpf)) {
			System.out.println("Usuário já cadastrado");
			return false;
		} else {
			workers.put(worker.cpf, worker);
			LinkedList<Events> list = new LinkedList<Events>();
			cardsPoint.put(worker.cpf, list );
			return true;
		}
	}

	public Workers searchCpf(String cpf) {
		if (workers.containsKey(cpf)) {
			return workers.get(cpf);
		}
		System.out.println("Nenhum usuário tem esse CPF");
		return null;
	}

	public boolean removeWorker(String cpf) {
		if (( searchCpf(cpf) ) != null) {
			return ( workers.remove(cpf) != null);
		} else {
			return false;
		}

	}

	public void cardsPoint( String cpf, Events events ){
		
		if( cardsPoint.containsKey(cpf) ){
			
			LinkedList<Events> list = cardsPoint.get(cpf);
			list.addLast(events);
			
		}
		
	}

	public void payment(){
		
		Set< String > keys = workers.keySet();
		System.out.println("Funcionario: " );
		for( String key : keys ){
			
			if( key != null ){
				Workers worker = workers.get(key);
				System.out.println( "CPF: " + key + " Nome: " + worker.name );
				if (worker instanceof Salaried) {
					
					System.out.println("Salário = " + worker.salary + " Deduções: " + worker.services );
					System.out.println(" Receber = " + ( worker.salary - worker.services )  );
					worker.salary = worker.services = 0;
					
				} else if( worker instanceof Seller ){
					
					Seller seller = (Seller)worker;
					System.out.println("Salário = " + seller.salary + " Comissão = " + seller.receive + " Deduções: " + seller.services );
					System.out.println(" Receber = " + ( seller.salary - seller.services + seller.receive )  );
					seller.receive = seller.services = 0;
					
				} else {
					
					Hourly hourly = (Hourly)worker;
					LinkedList<Events> points = cardsPoint.get(hourly.cpf);
					while( !points.isEmpty() ){
						
						if( points.size() == 1 ){
							break;
						} else {
							
							Events start = points.removeFirst();
							Events end = points.removeFirst();
							int timeWorked = start.variation(end);
							if( timeWorked > 8 ){
								hourly.receive += (timeWorked-8)*1.5*hourly.salary;
							}
							hourly.receive += hourly.salary*( Math.min(timeWorked, 8) );
						}
						
					}
					System.out.println("Salário por minuto = " + hourly.salary + " Receber = " + hourly.receive + " Deduções: " + hourly.services );
					System.out.println(" Receber = " + ( - hourly.services + hourly.receive )  );
					hourly.receive = hourly.services = 0;
					
				}
			}
			
		}
		
	}
	
}
